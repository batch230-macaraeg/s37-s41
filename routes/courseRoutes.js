const express = require("express");
const router = express.Router();
const courseControllers = require("../controllers/courseControllers.js");
const auth = require("../auth.js");

// Creating a course
// Activity
// 1. Update the "course"  route to implement "user authentication for the admin" when creating a course.

/*
router.post("/create", (request, res) => {
	courseControllers.addCourse(request.body).then(resultFromController => res.send(resultFromController))
})
*/

router.post("/create", (request, res) => {
	courseControllers.addCourse(request.body).then(resultFromController => res.send(resultFromController))
})

// Get all courses
router.get("/all", (request, response) => {
	courseControllers.getAllCourse().then(resultFromController => response.send(resultFromController))
})


// Get all ACTIVE courses
router.get("/active", (request, response) => {
	courseControllers.getActiveCourses().then(resultFromController => response.send(resultFromController))
})

// SPECIFIC course
router.get("/:courseId", (request, response) => {
	courseControllers.getCourse(request.params.courseId).then(resultFromController => response.send(resultFromController));
})

router.patch("/:courseId/update", auth.verify, (request,response) => 
{
	const newData = {
		course: request.body, 	//request.headers.authorization contains jwt
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	courseControllers.updateCourse(request.params.courseId, newData).then(resultFromController => {
		response.send(resultFromController)
	})
})

module.exports = router;

