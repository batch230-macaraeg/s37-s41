
const User = require("../models/User.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");


module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		/*
			// bcrypt - package for password hashing
			// hashSync - synchronously generate a hash
			// hash - asynchoursly generate a hash
		*/
		// hashing - converts a value to another value
		password: bcrypt.hashSync(reqBody.password, 10),
		/*
			// 10 = salt rounds
			// Salt rounds is proportional to hashing rounds, the higher the salt rounds, the more hashing rounds, the longer it takes to generate an output 
		*/
		mobileNo: reqBody.mobileNo
	})

	return newUser.save().then((user, error) => {
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}


module.exports.checkEmailExist = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0){
			return true;
		}
		else{
			return false;
		}
	})
}

module.exports.retrieveUserDetails = (reqBody) => {
	console.log(reqBody)
	return User.findById(reqBody._id).then(result => {
		if(result == null){
			return false;
		}
		else{
			result.password = "*****";
			return result;
		}
	})

}

module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null){
			return false
		}
		else{
			// compareSync is a bcrypt function to compare unhashed password to hashed password
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password); //true or false

			if(isPasswordCorrect){
				// Let's give the user a token to a access features
				return {access: auth.createAccessToken(result)};
			}
			else{
				// If password does not match, else
				return false;

			}
		}
	})
}


// s38 Activity
module.exports.getProfile = (reqBody) => {
	return User.findById(reqBody._id).then((result, err) => {
		if(err){
			return false;
		}
		else{
			result.password = "*****";
			return result;
		}
	})
}