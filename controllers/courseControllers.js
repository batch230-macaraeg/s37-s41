const mongoose = require("mongoose");
const Course = require("../models/course.js");

// Function for adding a course
// 2. Update the "addCourse" controller method to implement "Admin authentication" for creating a course.
module.exports.addCourse = (reqBody) => {

	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	})

	return newCourse.save().then((newCourse, error) =>
	{
		if(error){
			return error;
		}
		else{
			return newCourse;
		}
	})
}

// GET all course
module.exports.getAllCourse = () => {
	return Course.find({}).then(result => {
		return result;
	})
}

// GET all Active Courses
module.exports.getActiveCourses = () => {
	return Course.find({isActive:true}).then(result => {
		return result;
	})
}

module.exports.getCourse = (courseId) => {
	return Course.findById(courseId).then(result => {
		return result;
	})
}

// UPDATING a course
module.exports.updateCourse = (courseId, newData) => {
	if(newData.isAdmin == true){
		return Course.findByIdAndUpdate(courseId,
			{
				// newData.course.name
				// newData.request.body.name
				name: newData.course.name, 
				description: newData.course.description,
				price: newData.course.price
			}
		).then((result, error)=>{
			if(error){
				return false;
			}
			return true
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this functionality');
		return message.then((value) => {return value});
	}
}

// UPDATING a course for Activity 39
module.exports.updateCourse = (courseId, newData) => {
	if(newData.isAdmin == true){
		return Course.findByIdAndUpdate(courseId,
			{
				name: newData.course.name, 
				description: newData.course.description,
				price: newData.course.price
			}
		).then((result, error)=>{
			if(error){
				return false;
			}
			return true
		})
	}
	else{
		let message = Promise.resolve('ADMIN user can access this functionality');
		return message.then((value) => {return value});
	}
}
